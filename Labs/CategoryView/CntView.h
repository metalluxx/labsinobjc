//
//  CntView.h
//  Labs
//
//  Created by Metalluxx on 08/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../ApplicationSupport/CategoryViewProtocol.h"
#import "Contains.h"

NS_ASSUME_NONNULL_BEGIN




@interface CntView : NSView
// Wiev
@property (strong) IBOutlet NSView *contentsView;
// Buttons
@property (weak) IBOutlet NSButton *outletJur;
@property (weak) IBOutlet NSButton *outletGaz;
// Delegate
@property (nonatomic, weak) id<CategoryViewProtocol> delegate;

-(id)initWithFrame:(NSRect)frameRect;
-(id)initWithCoder:(NSCoder *)decoder;
-(void)setupUiBeforeInit;
-(void)willUpdatedContainsPtr: (id) sender;

- (IBAction)actionJur:(id)sender;
- (IBAction)actionGaz:(id)sender;
    
@end

NS_ASSUME_NONNULL_END
