//
//  CntView.m
//  Labs
//
//  Created by Metalluxx on 08/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "CntView.h"


@implementation CntView
@synthesize delegate;


-(id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self) {
        [self setupUiBeforeInit];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    self = [super initWithCoder:decoder];
    if (self) {
        [self setupUiBeforeInit];
    }
    return self;
}

-(void)setupUiBeforeInit{
    _contentsView = self;
    [[NSBundle mainBundle] loadNibNamed:@"CntView" owner:self topLevelObjects:nil];
    _contentsView.frame = [self bounds];
    [_contentsView setAutoresizingMask:NSViewHeightSizable];
    [self setAutoresizesSubviews:true];
    [self addSubview:_contentsView];
    
}

-(void)awakeFromNib{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willUpdatedContainsPtr:) name:@"ContainsControllerUpdatedObjectPtr" object:nil];
}

-(void)willUpdatedContainsPtr: (id) sender{
    if ([delegate toCurrentContains] == nil) {
        [_outletJur setEnabled:false];
        [_outletGaz setEnabled:false];
        [_outletJur setState:false];
        [_outletGaz setState:false];
    }
    else{
        [_outletJur setEnabled:true];
        [_outletGaz setEnabled:true];
        
        if([[delegate toCurrentContains]cat] == cNULL){
            [_outletJur setState:false];
            [_outletGaz setState:false];
        }
        if([[delegate toCurrentContains]cat] == cGAZETA){
            [_outletJur setState:false];
            [_outletGaz setState:true];
        }
        if([[delegate toCurrentContains]cat] == cJURNAL){
            [_outletJur setState:true];
            [_outletGaz setState:false];
        }
    }
    [delegate didClickedCategoryInUI:self];
}


- (IBAction)actionJur:(id)sender {
    [_outletGaz setState:false];
    [[delegate toCurrentContains] setCat:cJURNAL];
    [delegate didClickedCategoryInUI:self];
}

- (IBAction)actionGaz:(id)sender {
    [_outletJur setState:false];
    [[delegate toCurrentContains] setCat:cGAZETA];
    [delegate didClickedCategoryInUI:self];
}

@end
