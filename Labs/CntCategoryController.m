//
//  CntCategoryController.m
//  Labs
//
//  Created by Metalluxx on 08/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "CntCategoryController.h"
#import "Contains.h"



@protocol CntCategoryControllerProtocol
-(void) categoryChanged: (id) sender;
@end



@interface CntCategoryController ()
@property (strong) IBOutlet NSView *cntView;
@property (weak) IBOutlet NSButton *olJur;
@property (weak) IBOutlet NSButton *olGaz;
@property (nonatomic, weak)id<CntCategoryControllerProtocol> delegate;
@property (nonatomic) ContainsCategory currentCat;
@end

@implementation CntCategoryController
-(void)setCurrentCat:(ContainsCategory)currentCat{
    self.currentCat = currentCat;
    if(currentCat == cNULL){
        [_olJur setState:false];
        [_olGaz setState:false];
    }
    if(currentCat == cGAZETA){
        [_olJur setState:false];
        [_olGaz setState:true];
    }
    if(currentCat == cJURNAL){
        [_olJur setState:true];
        [_olGaz setState:false];
    }
    [_delegate categoryChanged:self];
}


- (IBAction)acJur:(id)sender {
    [_delegate categoryChanged:self];
}
- (IBAction)acGaz:(id)sender {
    [_delegate categoryChanged:self];
}

- (void)viewDidLoad {
    //[super viewDidLoad];
    // Do view setup here.
}

@end

