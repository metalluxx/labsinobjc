//
//  ContainsArrayAPI.m
//  Labs
//
//  Created by Metalluxx on 15/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "ContainsArrayAPI.h"


@implementation ContainsArrayAPI
@synthesize currentContains;
@synthesize delegate;

+(instancetype)sharedController{
    static ContainsArrayAPI* ptr;
    static dispatch_once_t once_ptr;
    
    dispatch_once(&once_ptr, ^{
        ptr = [[ContainsArrayAPI alloc] init];
    });
    
    return ptr;
}

-(instancetype) init{
    self = [super init];
    if (self) {
        containsArray = [[NSMutableDictionary alloc] init];
        observingKey = @"";
        [self addObserver:self forKeyPath:@"currentContains" options:0 context:nil];
        
    }
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"currentContains"]) {
        [delegate didChangedCurrentContainsPtr:self];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"ContainsControllerUpdatedObjectPtr" object:self];
    }
}

-(NSUInteger) sizeArray{
    return [containsArray count];
}

-(void) appendInArray:(Contains*) insertObj{
    [containsArray setValue:insertObj
                      forKey:[[NSNumber numberWithLong:random()] stringValue] ];
}

-(Contains*) getFromStringKey:(NSString*)key{
    return [containsArray valueForKey:key];
}

-(Contains*) getFromIndex:(NSUInteger) ind{
    NSString* key = [[containsArray allKeys] objectAtIndex:ind];
    return [containsArray valueForKey:key];
}


-(void) removeFromIndex:(NSUInteger) ind {
    NSString* key = [[containsArray allKeys] objectAtIndex:ind];
    [containsArray removeObjectForKey:key];
}

-(void) removeFromContain:(Contains*) cnt{
    NSUInteger ind = 0;
    for(Contains* sch in containsArray){
        if (sch == cnt) {
            [self removeFromIndex: ind];
            break;
        }
        ind++;
    }
}


@end
