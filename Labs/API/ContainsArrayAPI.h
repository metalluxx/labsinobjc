//
//  ContainsArrayAPI.h
//  Labs
//
//  Created by Metalluxx on 15/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../ApplicationSupport/Contains.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ContainsArrayAPIDelegate <NSObject>
-(void)didChangedCurrentContainsPtr:(id) sender;
@end


@interface ContainsArrayAPI : NSObject
{
    @private
    NSMutableDictionary* containsArray ;
    NSString* observingKey;
}
@property (nonatomic, weak) id<ContainsArrayAPIDelegate> delegate;
@property Contains* currentContains;

+(instancetype)sharedController;
-(NSUInteger) sizeArray;
-(void) appendInArray:(Contains*) insertObj;
-(Contains*) getFromStringKey:(NSString*)key;
-(Contains*) getFromIndex:(NSUInteger) ind;
-(void) removeFromIndex:(NSUInteger) ind;
-(void) removeFromContain:(Contains*) cnt;
@end

NS_ASSUME_NONNULL_END
