//
// Created by Metalluxx on 2019-02-16.
// Copyright (c) 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contains.h"

@protocol CategoryViewProtocol
@required
@property Contains* toCurrentContains;
-(void)didClickedCategoryInUI: (id) sender;
@end