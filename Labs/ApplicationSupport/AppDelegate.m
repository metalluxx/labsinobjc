//
//  AppDelegate.m
//  Labs
//
//  Created by Metalluxx on 06/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@end

@implementation AppDelegate


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}





@end
