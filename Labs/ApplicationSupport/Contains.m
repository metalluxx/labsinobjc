//
//  Contains.m
//  Lab
//
//  Created by Metalluxx on 05/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "Contains.h"


@implementation Contains
@synthesize name, index, money, cd, date,  cat, per, format;
-(instancetype) init{
    if(self = [super init]){
        self.name = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:rand()]];
        self.index = [NSNumber numberWithUnsignedInteger:10000];
        self.money = [NSNumber numberWithFloat:0.0];
        self.cd = false;
        self.date = [NSDate dateWithTimeIntervalSinceNow:0];
        self.cat = cNULL;
        self.per = pNULL;
        self.format = fNULL;
    }
    return self;
}
@end


@implementation Contains (Compare)
-(NSComparisonResult) compareIndex: (id) cmp{
    return [index compare:cmp];
}
-(NSComparisonResult) compareCategory: (id) cmp{
    return cat < cmp;
}
-(NSComparisonResult) comparePeriod: (id) cmp{
    return per < cmp;
}
@end


@implementation Contains (EncDec)
- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    [aCoder encodeObject:name forKey:@"name"];
    [aCoder encodeObject:index forKey:@"index"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedInteger:cat] forKey:@"cat"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedInteger:per] forKey:@"per"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedInteger:format] forKey:@"format"];
    [aCoder encodeObject:[NSNumber numberWithBool:cd] forKey:@"cd"];
    [aCoder encodeObject:date forKey:@"date"];
    [aCoder encodeObject:money forKey:@"money"];

}
- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    name = [aDecoder decodeObjectForKey:@"name"];
    index = [aDecoder decodeObjectForKey:@"index"];
    cat = (enum ContainsCategory) [[aDecoder decodeObjectForKey:@"cat"] integerValue];
    per = (enum ContainsPeriod) [[aDecoder decodeObjectForKey:@"per"] integerValue];
    format = (enum ContainsFormat) [[aDecoder decodeObjectForKey:@"format"] integerValue];
    cd = [[aDecoder decodeObjectForKey:@"cd"] boolValue];
    date = [aDecoder decodeObjectForKey:@"date"];
    money = [aDecoder decodeObjectForKey:@"money"];
    return  self;
}
@end

@implementation Contains (Copy)
- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    NSData* precp = [NSKeyedArchiver archivedDataWithRootObject:self];
    Contains* cp = [NSKeyedUnarchiver unarchiveObjectWithData:precp];
    return cp;
}
@end




