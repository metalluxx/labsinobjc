//
//  Contains.h
//  Lab
//
//  Created by Metalluxx on 05/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ContainsCategory) {
    cNULL, cJURNAL, cGAZETA
};

typedef NS_ENUM(NSInteger, ContainsPeriod) {
    pNULL = 0,
    pEVERYDAY = 1,
    pWEAK1 = 2,
    pWEAK2 = 3,
    pMONTH1 = 4,
    pYEAR6 = 5,
    pYEAR4 = 6,
    pYEAR2 = 7,
    pYEAR1 = 8
};

typedef NS_ENUM(NSInteger, ContainsFormat) {
    fNULL, fINFO, fMODA, fSPORT, fHEALTH, fGARDEN, fTRAVEL, fTV
};

@interface Contains : NSObject
@end

@interface Contains ()
@property  NSString* name;
@property  NSNumber* index;
@property  NSNumber* money;
@property  bool cd;
@property  NSDate* date;
@property  ContainsCategory cat;
@property  ContainsPeriod per;
@property  ContainsFormat format;
@end

@interface Contains (Compare)
-(NSComparisonResult) compareIndex: (id) cmp;
-(NSComparisonResult) compareCategory: (id) cmp;
-(NSComparisonResult) comparePeriod: (id) cmp;
@end


@interface Contains (Copy) <NSCopying>
@end



@interface Contains (EncDec) <NSCoding>
@end



NS_ASSUME_NONNULL_END
