//
// Created by Metalluxx on 2019-02-16.
// Copyright (c) 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contains.h"

@protocol PeriodViewProtocol
@required
@property Contains* toCurrentContains;
@end