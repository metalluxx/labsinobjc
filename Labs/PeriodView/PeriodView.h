//
//  PeriodView.h
//  Labs
//
//  Created by Metalluxx on 11/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../ApplicationSupport/Contains.h"
#import "../ApplicationSupport/PeriodViewProtocol.h"
#import "../CategoryView/CntView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PeriodView : NSView
{
    @private
    NSArray* periodsArr;
    bool statusEnabled;
}
@property (strong) IBOutlet NSView *contentView;
@property (nonatomic, weak) id<PeriodViewProtocol> delegate;

@property (weak) IBOutlet NSButton *outletEVERYDAY;
@property (weak) IBOutlet NSButton *outlet1WEAK;
@property (weak) IBOutlet NSButton *outlet2WEAK;
@property (weak) IBOutlet NSButton *outlet1MONTH;
@property (weak) IBOutlet NSButton *outlet6YEAR;
@property (weak) IBOutlet NSButton *outlet4YEAR;
@property (weak) IBOutlet NSButton *outlet2YEAR;
@property (weak) IBOutlet NSButton *outlet1YEAR;


-(IBAction)selectButtonPeriod:(id)sender;
-(void)setEnabled:(bool)se;

-(void)willUpdatedContainsPtr: (id) sender;
-(void)cleanPeriodViewUI;

@end


NS_ASSUME_NONNULL_END
