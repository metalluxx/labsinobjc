//
//  PeriodView.m
//  Labs
//
//  Created by Metalluxx on 11/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#define periodConvert(x) [NSNumber numberWithInteger:x]

#import "PeriodView.h"

@implementation PeriodView

-(id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self) {
        [self setupUiBeforeInit];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    self = [super initWithCoder:decoder];
    if (self) {
        [self setupUiBeforeInit];
    }
    return self;
}

-(void)setupUiBeforeInit{
    _contentView = self;
    [[NSBundle mainBundle] loadNibNamed:@"PeriodView" owner:self topLevelObjects:nil];
    _contentView.frame = [self bounds];
    [_contentView setAutoresizingMask:NSViewHeightSizable];
    [self setAutoresizesSubviews:true];
    [self addSubview:_contentView];
    
    statusEnabled = false;
}

-(void) awakeFromNib{
    periodsArr = [NSArray arrayWithObjects:_outletEVERYDAY,_outlet1WEAK, _outlet2WEAK, _outlet1MONTH, _outlet6YEAR, _outlet4YEAR, _outlet2YEAR, _outlet1YEAR, nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpdatedContainsPtr) name:@"ContainsControllerUpdatedObjectPtr" object:nil];
}


// Enabling-disabling UI r-buttons
-(void)setEnabled:(bool)se{
    if( se != statusEnabled ){
        for (NSButton* srh in periodsArr){
            [srh setEnabled:se];
        }
    }
    statusEnabled = se;
}

-(void)cleanPeriodViewUI{
    if([_delegate toCurrentContains] == nil){
        [self setEnabled:false];
        for (NSButton* srh in periodsArr){
            [srh setState:NSControlStateValueOff];
        }
        return;
    }
    // if have contains_ptr
    [self setEnabled:true];
    for (NSButton* srh in periodsArr){
        [srh setState:NSControlStateValueOff];
    }
}


-(void)willUpdatedContainsPtr: (id) sender{

    [self cleanPeriodViewUI];
    
    for(NSButton* srh in periodsArr){

        if ([[srh identifier] integerValue] == [[_delegate toCurrentContains] per]) {
            [srh setState:NSControlStateValueOn];
            break;
        }
    }
}


// Hot write data in ptr
-(IBAction)selectButtonPeriod:(id)sender{
    [[_delegate toCurrentContains] setPer:(ContainsPeriod)[[sender identifier] integerValue]];
}

@end
