//
//  ViewController.m
//  Labs
//
//  Created by Metalluxx on 06/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize dateOutlet;
@synthesize cdOutlet;
@synthesize sliderOutlet;
@synthesize toCurrentContains;

@synthesize minValueMoney;
@synthesize maxValueMoney;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setup enviroment
    arrayApi = [ContainsArrayAPI sharedController];
    [arrayApi setDelegate:self];
    [_categoryView setDelegate:self];
    [_periodView setDelegate:self];
    
    // Add observing: sync local and api currentContains
    [arrayApi addObserver:self forKeyPath:@"currentContains" options:0 context:nil];

//     TextField support
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectStringForTextField:) name:@"ContainsControllerUpdatedObjectPtr" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(correctDateUI) name:@"ContainsControllerUpdatedObjectPtr" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(correctCdUI) name:@"ContainsControllerUpdatedObjectPtr" object:nil];
    
    // Toolbar support
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addContainsToTable:) name:@"AddToolBarItemClicked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteContainsToTable:) name:@"DeleteToolBarItemClicked" object:nil];
    // Change current object ptr
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"ContainsControllerUpdatedObjectPtr" object:self];

    // Setup UI visual
    [self didChangedCurrentContainsPtr:self];
    
    // Do any additional setup after loading the view.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if (([keyPath isEqualToString:@"currentContains"]) && (object == arrayApi) ) {
        //Update ptr
        toCurrentContains = arrayApi.currentContains;
    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    // Update the view, if already loaded.
}
@end



@implementation ViewController (ApiDelegate)

- (void)didChangedCurrentContainsPtr:(id)sender{

    // Update UI before write new ptr cc
    [self correctTextFieldUI:sender];
    [self correctDateUI];
    [self correctCdUI];
    [_categoryView willUpdatedContainsPtr:sender];
    [_periodView willUpdatedContainsPtr:sender];
}
@end




@implementation ViewController (TextFieldSupport)

-(void) correctTextFieldUI: (id) sender{

    if([arrayApi currentContains] == nil){
        [_textField setStringValue:@""];
        [_textField setEnabled:false];
    }
    else{
        [_textField setEnabled:true];
        [_textField setStringValue:[[arrayApi currentContains] name]];
    }
}

//this delegate method
- (void)controlTextDidChange:(NSNotification *)notification {
    if ([arrayApi currentContains] != nil) {

        if([[[notification object] identifier]  isEqual: @"IndexName"]){
            [[arrayApi currentContains] setValue:[[notification object] stringValue] forKey:@"name"];
            [_tableView
             reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:[_tableView selectedRow]]
             columnIndexes:[NSIndexSet indexSetWithIndex:0]];
        }
    }
}
@end




@implementation ViewController (CheckBoxSupport)
-(IBAction)didClickedCdInUI:(id)sender{
    [[arrayApi currentContains] setCd:(bool)[cdOutlet state]];
}
-(void) correctCdUI{
    if([arrayApi currentContains] != nil){
        [cdOutlet setEnabled:true];
        [cdOutlet setState:[[arrayApi currentContains] cd]];
    }
    else{
        [cdOutlet setEnabled:false];
        [cdOutlet setState:0];
    }
}
@end

@implementation ViewController (DateSupport)
-(IBAction)didDateEditedInUI:(id)sender{
    [[arrayApi currentContains] setValue:[dateOutlet dateValue] forKey:@"date"] ;
}

-(void) correctDateUI{
    if([dateOutlet dateValue] != nil){
        [dateOutlet setEnabled:true];
        [dateOutlet setDateValue:[[arrayApi currentContains] date]];
    }
    else{
        [dateOutlet setEnabled:false];
        [dateOutlet setDateValue:[NSDate dateWithTimeIntervalSinceNow:0]];
    }
}
@end


@implementation ViewController (ToolBarSupport)

-(IBAction)addContainsToTable:(id)sender{
    Contains* new = [[Contains alloc]init];
//    [containsArr addObject:new];
    [arrayApi appendInArray:new];
    [_tableView reloadData];
}

-(IBAction)deleteContainsToTable:(id)sender{

    if(arrayApi.sizeArray == 0) return;

    NSUInteger bck = [_tableView selectedRow];

    [_tableView beginUpdates];
//    [containsArr removeObjectAtIndex:bck];
    [arrayApi removeFromIndex:bck];
    [_tableView removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:bck] withAnimation:NSTableViewAnimationEffectGap];
    if ([_tableView numberOfRows] != 0) {
        if(bck == [_tableView numberOfRows]) bck--;
        [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:bck] byExtendingSelection:false];
    }
    [_tableView reloadData];
    [_tableView endUpdates];

    if(arrayApi.sizeArray == 0) {
        [arrayApi setValue:nil forKey:@"currentContains"] ;
    }
}
@end


@implementation ViewController (Table)

//this delegate method
-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    return [arrayApi sizeArray];
}

//this delegate method
-(id) tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    Contains *gt = [arrayApi getFromIndex:row];
    return [NSString stringWithFormat:@"%@ | %@" , gt.name, gt.money ];
}

//this delegate method
- (void)tableViewSelectionDidChange:(NSNotification *)notification{
    [arrayApi setValue:[arrayApi getFromIndex:[_tableView selectedRow]] forKey:@"currentContains"] ;
}

@end


@implementation ViewController (MoneySupport) // this support used Cocoa bindings

- (IBAction)didMoneySliderEditedInUI:(id)sender {
    [_tableView
     reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:[_tableView selectedRow]]
     columnIndexes:[NSIndexSet indexSetWithIndex:0]];
}

-(void)didClickedCategoryInUI:(id)sender{
    [self updateRangeValueForMoney];
}

-(void) updateRangeValueForMoney{
    if ([toCurrentContains cat] == cNULL) {
        self.minValueMoney = 0;
        self.maxValueMoney = 0;
        [sliderOutlet setFloatValue:0];
    }
    if ([toCurrentContains cat] == cJURNAL) {
        self.minValueMoney = 1500;
        self.maxValueMoney = 50000;
        [self correctionMoneyValueInRange];
    }
    if ([toCurrentContains cat] == cGAZETA) {
        self.minValueMoney = 500;
        self.maxValueMoney = 5000;
        [self correctionMoneyValueInRange];
    }
    [_tableView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:[_tableView selectedRow]] columnIndexes:[NSIndexSet indexSetWithIndex:0]];
}

- (void)correctionMoneyValueInRange {
    float currentMoney = [[[arrayApi currentContains]money] floatValue];
    if ((currentMoney > maxValueMoney) || (currentMoney < minValueMoney)) {
            [[arrayApi currentContains] setMoney:@(minValueMoney)];
        }
}

@end
