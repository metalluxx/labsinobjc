//
//  ViewController.h
//  Labs
//
//  Created by Metalluxx on 06/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../CategoryView/CntView.h"
#import "../PeriodView/PeriodView.h"
#import "../API/ContainsArrayAPI.h"
#import "../ApplicationSupport/CategoryViewProtocol.h"
#import "../ApplicationSupport/PeriodViewProtocol.h"


@interface ViewController : NSViewController <CategoryViewProtocol, PeriodViewProtocol>
{
    ContainsArrayAPI* arrayApi;
}
@property (weak) IBOutlet CntView *categoryView;
@property (weak) IBOutlet PeriodView *periodView;

@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSTableColumn *tableColumn;

@property (weak) IBOutlet NSDatePicker *dateOutlet;
@property (weak) IBOutlet NSTextField *textField;
@property (weak) IBOutlet NSButton *cdOutlet;
@property IBOutlet NSSlider* sliderOutlet;


@property Contains* toCurrentContains;
@property float minValueMoney;
@property float maxValueMoney;
@end


@interface ViewController (ToolBarSupport)
-(IBAction)addContainsToTable:(id)sender;
-(IBAction)deleteContainsToTable:(id)sender;
@end


@interface ViewController (TextFieldSupport) <NSTextFieldDelegate>
-(void) correctTextFieldUI: (id) sender;
@end


@interface ViewController (CheckBoxSupport)
-(IBAction)didClickedCdInUI:(id)sender;
-(void) correctCdUI;
@end


@interface ViewController (DateSupport)
-(IBAction)didDateEditedInUI:(id)sender;
-(void) correctDateUI;
@end


@interface ViewController (Table) <NSTableViewDataSource, NSTableViewDelegate>
// methods from delegate protocols
@end


@interface ViewController (ApiDelegate) <ContainsArrayAPIDelegate>
// methods from delegate protocol
@end


@interface ViewController (MoneySupport)
-(void) updateRangeValueForMoney;
@end
