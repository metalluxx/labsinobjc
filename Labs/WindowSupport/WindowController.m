//
//  WindowController.m
//  Labs
//
//  Created by Metalluxx on 11/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "WindowController.h"


@implementation WindowController
@synthesize addActionOutlet, deleteActionOutlet;

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end

@implementation WindowController (ToolbarNotification)

- (void)windowWillClose:(NSNotification *)notification{
    [NSApp terminate:self];
}

-(IBAction)didClickedDeleteToolBarItem:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteToolBarItemClicked" object:self];
}


-(IBAction)didClickedAddToolBarItem:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddToolBarItemClicked" object:self];
}

@end


