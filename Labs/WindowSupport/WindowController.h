//
//  WindowController.h
//  Labs
//
//  Created by Metalluxx on 11/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WindowController : NSWindowController
@property (weak) IBOutlet NSToolbarItem *addActionOutlet;
@property (weak) IBOutlet NSToolbarItem *deleteActionOutlet;


@end

@interface WindowController (ToolbarNotification)
-(IBAction)didClickedDeleteToolBarItem:(id)sender;
-(IBAction)didClickedAddToolBarItem:(id)sender;
@end



NS_ASSUME_NONNULL_END
